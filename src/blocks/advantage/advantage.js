$(window).on('load resize', function () {

  if ($(window).width() >= 1200) {

    var maxHeight = Math.max.apply(null, $(".advantage").map(function () {
      return $(this).height();
    }).get());
    $('.advantage').css('height', maxHeight);
  }
  
  else {
    $('.advantage').css('height', 'auto');
  }
});
